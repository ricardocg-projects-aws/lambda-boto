provider "aws" {
  region = "us-east-1"
}

module "lambda" {
    source                          = "git::https://gitlab.com/ricardocg-tf-aws/tf-lambda-module.git?ref=master"
    name                            = "Lambda-test"
    description                     = "Test"
    filename                        = "./src/lambda.zip"
    handler                         = "lambda_function.list_s3_buckets"
    runtime                         = "python3.7"
    env                             = "Test"

}